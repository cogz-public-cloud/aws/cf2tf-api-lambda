output "endpoint8024_a810" {
  value = join("", ["https://", aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id, ".execute-api.", data.aws_region.current.name, ".", data.aws_partition.current.dns_suffix, "/", aws_api_gateway_stage.endpoint_deployment_stageprod_b78_beea0.arn, "/"])
}

