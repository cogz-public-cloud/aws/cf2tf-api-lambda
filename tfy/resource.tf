resource "aws_iam_role" "hello_handler_service_role11_ef7_c63" {
  assume_role_policy = {
    Statement = [{'Action': '"sts:AssumeRole"', 'Effect': '"Allow"', 'Principal': {'Service': '"lambda.amazonaws.com"'}}]
    Version = "2012-10-17"
  }
  managed_policy_arns = ['join("", ["arn:", data.aws_region.current.name, ":iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"])']
}

resource "aws_lambda_function" "hello_handler2_e4_fba4_d" {
  code_signing_config_arn = {
    S3Bucket = "cdk-hnb659fds-assets-${data.aws_region.current.name}-${data.aws_region.current.name}"
    S3Key = "915054fc5a2f205a6a36929dbab5721b6991ba60e375ce8c968bd75690d53b16.zip"
  }
  role = aws_iam_role.hello_handler_service_role11_ef7_c63.arn
  handler = "hello.handler"
  runtime = "python3.7"
}

resource "aws_api_gateway_rest_api" "endpoint_eef1_fd8_f" {
  name = "Endpoint"
}

resource "aws_iam_role" "endpoint_cloud_watch_role_c3_c64_e0_f" {
  assume_role_policy = {
    Statement = [{'Action': '"sts:AssumeRole"', 'Effect': '"Allow"', 'Principal': {'Service': '"apigateway.amazonaws.com"'}}]
    Version = "2012-10-17"
  }
  managed_policy_arns = ['join("", ["arn:", data.aws_region.current.name, ":iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"])']
}

resource "aws_api_gateway_account" "endpoint_account_b8304247" {
  cloudwatch_role_arn = aws_iam_role.endpoint_cloud_watch_role_c3_c64_e0_f.arn
}

resource "aws_api_gateway_deployment" "endpoint_deployment318525_da808eeef0ae3eb0745f2faa622f752de3" {
  rest_api_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id
  description = "Automatically created by the RestApi construct"
}

resource "aws_api_gateway_stage" "endpoint_deployment_stageprod_b78_beea0" {
  rest_api_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id
  deployment_id = aws_api_gateway_deployment.endpoint_deployment318525_da808eeef0ae3eb0745f2faa622f752de3.id
  stage_name = "prod"
}

resource "aws_api_gateway_resource" "endpointproxy39_e2174_e" {
  parent_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.root_resource_id
  path_part = "{proxy+}"
  rest_api_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id
}

resource "aws_lambda_permission" "endpointproxy_any_api_permissioncdkworkshop_endpoint424_a4_d39_an_yproxy_ed9_f30_e3" {
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello_handler2_e4_fba4_d.arn
  principal = "apigateway.amazonaws.com"
  source_arn = join("", ["arn:", data.aws_region.current.name, ":execute-api:", data.aws_region.current.name, ":", data.aws_region.current.name, ":", aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id, "/", aws_api_gateway_stage.endpoint_deployment_stageprod_b78_beea0.arn, "/*/*"])
}

resource "aws_lambda_permission" "endpointproxy_any_api_permission_testcdkworkshop_endpoint424_a4_d39_an_yproxy4_fb922_c2" {
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello_handler2_e4_fba4_d.arn
  principal = "apigateway.amazonaws.com"
  source_arn = join("", ["arn:", data.aws_region.current.name, ":execute-api:", data.aws_region.current.name, ":", data.aws_region.current.name, ":", aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id, "/test-invoke-stage/*/*"])
}

resource "aws_api_gateway_method" "endpointproxy_anyc09721_c5" {
  http_method = "ANY"
  resource_id = aws_api_gateway_resource.endpointproxy39_e2174_e.id
  rest_api_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id
  authorization = "NONE"
  operation_name = {
    IntegrationHttpMethod = "POST"
    Type = "AWS_PROXY"
    Uri = join("", ["arn:", data.aws_region.current.name, ":apigateway:", data.aws_region.current.name, ":lambda:path/2015-03-31/functions/", aws_lambda_function.hello_handler2_e4_fba4_d.arn, "/invocations"])
  }
}

resource "aws_lambda_permission" "endpoint_any_api_permissioncdkworkshop_endpoint424_a4_d39_anyc722176_d" {
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello_handler2_e4_fba4_d.arn
  principal = "apigateway.amazonaws.com"
  source_arn = join("", ["arn:", data.aws_region.current.name, ":execute-api:", data.aws_region.current.name, ":", data.aws_region.current.name, ":", aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id, "/", aws_api_gateway_stage.endpoint_deployment_stageprod_b78_beea0.arn, "/*/"])
}

resource "aws_lambda_permission" "endpoint_any_api_permission_testcdkworkshop_endpoint424_a4_d39_anyb0_c9_fb02" {
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello_handler2_e4_fba4_d.arn
  principal = "apigateway.amazonaws.com"
  source_arn = join("", ["arn:", data.aws_region.current.name, ":execute-api:", data.aws_region.current.name, ":", data.aws_region.current.name, ":", aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id, "/test-invoke-stage/*/"])
}

resource "aws_api_gateway_method" "endpoint_any485_c938_b" {
  http_method = "ANY"
  resource_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.endpoint_eef1_fd8_f.id
  authorization = "NONE"
  operation_name = {
    IntegrationHttpMethod = "POST"
    Type = "AWS_PROXY"
    Uri = join("", ["arn:", data.aws_region.current.name, ":apigateway:", data.aws_region.current.name, ":lambda:path/2015-03-31/functions/", aws_lambda_function.hello_handler2_e4_fba4_d.arn, "/invocations"])
  }
}

resource "aws_ecs_task_set" "cdk_metadata" {
  // CF Property(Analytics) = "v2:deflate64:H4sIAAAAAAAA/1WP0U7DMAxFv2XvqWEbEs/dEE8gqvIBU5aa4rVJqtrRqKr+O0kKYjz5+FrX197B7gG2G33lwjRd0dMZ5nfRplNROs29tudGw/wcnBHyTh0/3C1XOFpijt2iSFuYa99jGqS6KN6fNDMKQ5lK7OEQTIdy0IxKD9RqwaueYH7JQTWylAPlBX9YGuODE/WEQ+8nixGjetPFg9ucukK0+jAazCHV6L+mX+Vn8cqvKJ++SdJKy6LylWkJuTYN3oIMQf65Ih+9a0jyz9UUne5uD4+wvd9cmKgY46lkEeq1fgNEJNSPYQEAAA=="
}

